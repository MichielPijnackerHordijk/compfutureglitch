package com.glitch.worldpopulation.repository

data class CountryList (
    val countries: List<String>
)
