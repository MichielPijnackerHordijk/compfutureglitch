package com.glitch.worldpopulation.repository

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("/1.0/countries")
    fun countries(): Call<CountryList>
}