/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.glitch.worldpopulation.ui.adapter

import android.support.annotation.MainThread
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.glitch.worldpopulation.AppExecutors
import com.glitch.worldpopulation.ui.common.DiffUtilCallback
import com.glitch.worldpopulation.ui.common.NavigationController
import com.glitch.worldpopulation.ui.common.StringDiffCalculator
import com.glitch.worldpopulation.ui.common.StringDiffUtilCallback
import com.glitch.worldpopulation.ui.viewholder.CountryViewHolder
import com.glitch.worldpopulation.ui.viewholder.DataBoundViewHolder

/**
 * A generic RecyclerView adapt er that uses Data Binding & DiffUtil.
 *
 * @param <DataBindingComponent> The of the ViewDataBinding
</DataBindingComponent> */
class CountryAdapter(private val appExecutors: AppExecutors, private val navigationController: NavigationController) :
        RecyclerView.Adapter<CountryViewHolder>(), StringDiffUtilCallback {
    private val onCountryClick: (String) -> Unit = { navigationController.navigateToCountry(it) }

    private var items: List<String> = emptyList()

    // Each time data is set, we update this variable so that if DiffUtil calculation returns
    // after repetitive updates, we can ignore the old calculation
    private var dataVersion = 0

    init {
        this.setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        return CountryViewHolder(parent, onCountryClick)

    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.country = items[position]
    }


    override fun getItemCount() = items.size

    override fun getItemId(position: Int): Long {
        return items[position].hashCode().toLong()
    }

    @MainThread
    fun replaceItems(update: List<String>?) {
        dataVersion++

        if (update == null || update.isEmpty()) {
            val oldSize = items.size
            items = emptyList()
            notifyItemRangeRemoved(0, oldSize)
        } else {
            val startVersion = dataVersion
            val oldItems = items

            appExecutors.forHighPriority {
                val result = createDiffResult(oldItems, update, this)
                appExecutors.onUi {
                    if (startVersion == dataVersion) {
                        result.dispatchUpdatesTo(this@CountryAdapter)
                        items = update
                    }
                }
            }
        }
    }

    open fun createDiffResult(oldItems: List<String>, update: List<String>,
                              callback: DiffUtilCallback<String>): DiffUtil.DiffResult =
            DiffUtil.calculateDiff(StringDiffCalculator(oldItems, update, callback), false)
}