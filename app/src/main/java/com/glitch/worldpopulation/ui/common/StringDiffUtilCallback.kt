package com.glitch.worldpopulation.ui.common

/**
 * Created by glitch on 21-3-18.
 */
interface StringDiffUtilCallback : DiffUtilCallback<String> {
    override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }

    override fun getChangePayload(oldItem: String, newItem: String): Any? = null
}