package com.glitch.worldpopulation.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.glitch.worldpopulation.repository.PopulationRepository
import javax.inject.Inject


class ListViewModel @Inject
internal constructor(populationRepository: PopulationRepository) : ViewModel() {

    val countries: LiveData<List<String>>

    init {
        countries = populationRepository.countries
   }
}