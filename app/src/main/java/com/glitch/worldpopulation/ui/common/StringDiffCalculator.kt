package com.glitch.worldpopulation.ui.common

class StringDiffCalculator(
        oldItems: List<String>,
        update: List<String>,
        callback: DiffUtilCallback<String> = object: StringDiffUtilCallback{}
)
    : DiffCalculator<String>(oldItems, update, callback)