package com.glitch.worldpopulation.ui.common

import android.support.v7.util.DiffUtil

open class DiffCalculator<T>(
        private val oldItems: List<T>,
        private val update: List<T>,
        private val callback: DiffUtilCallback<T>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldItems.size
    override fun getNewListSize() = update.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return callback.areItemsTheSame(oldItems[oldItemPosition], update[newItemPosition])
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return callback.areContentsTheSame(oldItems[oldItemPosition], update[newItemPosition])
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        return callback.getChangePayload(oldItems[oldItemPosition], update[newItemPosition])
    }
}