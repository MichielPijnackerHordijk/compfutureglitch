package com.glitch.worldpopulation.extensions

/**
 * Calls the specified function [block] with `this` value as its receiver and returns `this` value,
 * but only when predicate is true
 */
inline fun <T> T.runIf(predicate: Boolean, block: T.() -> T): T {
    if (!predicate) return this
    return block()
}